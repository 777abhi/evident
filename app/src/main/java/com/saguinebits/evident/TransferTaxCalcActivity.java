package com.saguinebits.evident;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Prince on 19-03-2016.
 */
public class TransferTaxCalcActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.etSaleAmount)
    EditText etSaleAmount;
    @BindView(R.id.tvExemption)
    TextView tvExemption;
    @BindView(R.id.tvPropertyType)
    TextView tvPropertyType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_transfer_tax_calculator);
        ButterKnife.bind(this);

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.transfer_tax_calculator));

        Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_back);
        upArrow.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick({R.id.tvExemption, R.id.tvPropertyType, R.id.tvCalculate})
    void click(View view) {
        switch (view.getId()) {
            case R.id.tvExemption:
                openExemptionListPopupWindow();
                break;
            case R.id.tvPropertyType:
                openPropertyTypeListPopupWindow();
                break;
            case R.id.tvCalculate:

                if (etSaleAmount.getText().toString().equals("")) {
                    showToast("Please enter Sale Amount.");
                } else if (tvExemption.getText().toString().equals("")) {
                    showToast("Please select Exemption.");
                } else if (tvPropertyType.getText().toString().equals("")) {
                    showToast("Please select Property Type.");
                } else {
                    double amount = Double.parseDouble(etSaleAmount.getText().toString());
                    double transfer_tax_amount = calculateTransferTaxAmount(amount);
                    startActivity(new Intent(this, TransferTaxCalcResultActivity.class)
                            .putExtra(TransferTaxCalcResultActivity.EXTRA_SALE_AMOUNT, roundOffTo2DecPlaces(amount))
                            .putExtra(TransferTaxCalcResultActivity.EXTRA_EXEMPTION, tvExemption.getText().toString())
                            .putExtra(TransferTaxCalcResultActivity.EXTRA_PROPERTY_TYPE, tvPropertyType.getText().toString())
                            .putExtra(TransferTaxCalcResultActivity.EXTRA_TRANSFER_TAX_AMOUNT, roundOffTo2DecPlaces(transfer_tax_amount)));
                }
                break;
            default:
                break;
        }
    }

//    private double calculateTransferTaxAmount(double amount) {
//        if (tvExemption.getText().toString().equalsIgnoreCase("No Exemption")) {
//            return amount * 0.004;
//        } else {
//            return amount * 0.001;
//        }
//    }

    private double calculateTransferTaxAmount(double amount) {
        if (tvExemption.getText().toString().equalsIgnoreCase("No Exemption")) {
            if (amount <= 350000) {
                if (amount < 150000) {
                    return amount * 0.004;
                } else if (amount < 200000) {
                    return 150000 * 0.004 + (amount - 150000) * 0.0067;
                } else {
                    return 150000 * 0.004 + 50000 * 0.0067 + (amount - 200000) * 0.0078;
                }
            } else {
                if (amount < 150000) {
                    return amount * 0.0058;
                } else if (amount < 200000) {
                    return 150000 * 0.0058 + (amount - 150000) * 0.0085;
                } else if (amount < 550000) {
                    return 150000 * 0.0058 + 50000 * 0.0085 + (amount - 200000) * 0.0096;
                } else if (amount < 850000) {
                    return 150000 * 0.0058 + 50000 * 0.0085 + 350000 * 0.0096 + (amount - 550000) * 0.0106;
                } else if (amount < 1000000) {
                    return 150000 * 0.0058 + 50000 * 0.0085 + 350000 * 0.0096 + 300000 * 0.0106 + (amount - 850000) * 0.0116;
                } else {
                    return 150000 * 0.0058 + 50000 * 0.0085 + 350000 * 0.0096 + 300000 * 0.0106 + 150000 * 0.0116 + (amount - 1000000) * 0.0121;
                }
            }
        } else {
            if (amount <= 350000) {
                if (amount < 150000) {
                    return amount * 0.001;
                } else {
                    return 150000 * 0.001 + (amount - 150000) * 0.0025;
                }
            } else {
                if (amount < 150000) {
                    return amount * 0.0028;
                } else if (amount < 550000) {
                    return 150000 * 0.0028 + (amount - 150000) * 0.0043;
                } else if (amount < 850000) {
                    return 150000 * 0.0028 + 400000 * 0.0043 + (amount - 550000) * 0.0053;
                } else if (amount < 1000000) {
                    return 150000 * 0.0028 + 400000 * 0.0043 + 300000 * 0.0053 + (amount - 850000) * 0.0063;
                } else {
                    return 150000 * 0.0028 + 400000 * 0.0043 + 300000 * 0.0053 + 150000 * 0.0063 + (amount - 1000000) * 0.0068; // little doubt in 0.0068 at end
                }
            }
        }
    }

    private void openExemptionListPopupWindow() {
        final String[] exemptions = {"No Exemption", "Senior Citizen", "Disabled/Blind", "Low Income Housing"};
        final ListPopupWindow listPopupWindow = new ListPopupWindow(this);
        listPopupWindow.setAdapter(new ArrayAdapter(this, R.layout.row_popuplist, exemptions));
        listPopupWindow.setAnchorView(tvExemption);
        listPopupWindow.setWidth(tvExemption.getWidth());
        listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);
        listPopupWindow.setModal(true);
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                tvExemption.setText(exemptions[i]);
                listPopupWindow.dismiss();
            }
        });
        listPopupWindow.show();
    }

    private void openPropertyTypeListPopupWindow() {
        final String[] property_types = {"Residential", "Commercial"};
        final ListPopupWindow listPopupWindow = new ListPopupWindow(this);
        listPopupWindow.setAdapter(new ArrayAdapter(this, R.layout.row_popuplist, property_types));
        listPopupWindow.setAnchorView(tvPropertyType);
        listPopupWindow.setWidth(tvPropertyType.getWidth());
        listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);
        listPopupWindow.setModal(true);
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                tvPropertyType.setText(property_types[i]);
                listPopupWindow.dismiss();
            }
        });
        listPopupWindow.show();
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    String roundOffTo2DecPlaces(double val) {
        return String.format("%.2f", val);
    }
}
