package com.saguinebits.evident;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.RadioGroup;

import com.saguinebits.evident.fragments.CalculatorFragment;
import com.saguinebits.evident.fragments.EventsFragment;
import com.saguinebits.evident.fragments.HomeFragment;
import com.saguinebits.evident.fragments.MoreFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Prince on 18-03-2016.
 */
public class MainActivity extends AppCompatActivity {

    @BindView(R.id.rg)
    RadioGroup radioGroup;

    private Unbinder unbinder;
// midtown,classic
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.setDebug(true);
        unbinder = ButterKnife.bind(this);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fl_fragment, new HomeFragment());
        transaction.commit();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                switch (i) {
                    case R.id.rbHome:
                        transaction.replace(R.id.fl_fragment, new HomeFragment());
                        break;
                    case R.id.rbCalculator:
                        transaction.replace(R.id.fl_fragment, new CalculatorFragment());
                        break;
                    case R.id.rbEvents:
                        transaction.replace(R.id.fl_fragment, new EventsFragment());
                        break;
                    case R.id.rbMore:
                        transaction.replace(R.id.fl_fragment, new MoreFragment());
                        break;
                    default:
                        break;
                }
                transaction.commit();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
