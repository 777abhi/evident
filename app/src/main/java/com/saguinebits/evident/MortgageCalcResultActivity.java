package com.saguinebits.evident;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Prince on 19-03-2016.
 */
public class MortgageCalcResultActivity extends AppCompatActivity {

    public static String EXTRA_LOAN_AMOUNT = "extra_loan_amount";
    public static String EXTRA_INTEREST_RATE = "extra_interest_rate";
    public static String EXTRA_TERM_YEARS = "extra_term_years";
    public static String EXTRA_YEARLY_TAXES = "extra_yearly_taxes";
    public static String EXTRA_YEARLY_INSURANCE = "extra_yearly_insurance";
    public static String EXTRA_MONTHLY_PAYMENT = "extra_monthly_payment";
    public static String EXTRA_TOTAL_MONTHLY_PAYMENT = "extra_total_monthly_payment";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvRight)
    TextView tvRight;
    @BindView(R.id.tvTotalMonthlyPaymentResult)
    TextView tvTotalMonthlyPaymentResult;

    private String loan_amount = "", interest_rate = "", term = "", yearly_taxes = "", yearly_insurance = "", monthly_payment = "", total_monthly_payment = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_result_mortgage_calc);
        ButterKnife.bind(this);

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.mortgage_calculator));

        Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_back);
        upArrow.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loan_amount = getIntent().getStringExtra(EXTRA_LOAN_AMOUNT);
        interest_rate = getIntent().getStringExtra(EXTRA_INTEREST_RATE);
        term = getIntent().getStringExtra(EXTRA_TERM_YEARS);
        yearly_taxes = getIntent().getStringExtra(EXTRA_YEARLY_TAXES);
        yearly_insurance = getIntent().getStringExtra(EXTRA_YEARLY_INSURANCE);
        monthly_payment = getIntent().getStringExtra(EXTRA_MONTHLY_PAYMENT);
        total_monthly_payment = getIntent().getStringExtra(EXTRA_TOTAL_MONTHLY_PAYMENT);

        tvRight.setText("$" + loan_amount + "\n" + interest_rate + "%\n" + term + " Years\n$ " + monthly_payment + "\n$" + yearly_taxes + "\n$" + yearly_insurance);
        tvTotalMonthlyPaymentResult.setText("$" + total_monthly_payment);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.tvEmailEstimates)
    void click() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "", null));
        emailIntent.putExtra(Intent.EXTRA_BCC,new String[] { "info@evidenttitle.com","quote@evidenttitle.com"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.email_subject_mortgage_calc_result));
        emailIntent.putExtra(Intent.EXTRA_TEXT,
                getResources().getString(R.string.email_result_motgage) +
                        "\n\n" +
                        "Loan Amount: $" + loan_amount +
                        "\n" +
                        "Interest Rate: " + interest_rate + "%" +
                        "\n" +
                        "Term: " + term + " Years" +
                        "\n" +
                        "Yearly Taxes: $" + yearly_taxes +
                        "\n" +
                        "Yearly Insurance: $" + yearly_insurance +
                        "\n" +
                        "Total Monthly Payment: $" + total_monthly_payment +
                        "\n\n" +
                        getResources().getString(R.string.disclaimer_mortgage_calc));
        startActivity(Intent.createChooser(emailIntent, "Send email..."));

    }

}
