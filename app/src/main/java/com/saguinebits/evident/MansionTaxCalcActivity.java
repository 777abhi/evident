package com.saguinebits.evident;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Prince on 23-03-2016.
 */
public class MansionTaxCalcActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.etSaleAmount)
    EditText etSaleAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_mansion_tax_calculator);
        ButterKnife.bind(this);

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.mansion_tax_calculator));

        Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_back);
        upArrow.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.tvCalculate)
    void click() {
        if (etSaleAmount.getText().toString().equals("")) {
            showToast("Please enter Sale Amount.");
        } else {
            double amount = Double.parseDouble(etSaleAmount.getText().toString());
            if (amount > 1000000) {
                double mansion_tax_amount = calculateMansionTaxAmount(amount);
                startActivity(new Intent(this, MansionTaxCalcResultActivity.class)
                        .putExtra(MansionTaxCalcResultActivity.EXTRA_SALE_AMOUNT, roundOffTo2DecPlaces(amount))
                        .putExtra(MansionTaxCalcResultActivity.EXTRA_MANSION_TAX_AMOUNT, roundOffTo2DecPlaces(mansion_tax_amount)));
            } else {
                showToast("Please enter Sale Amount greater than $1 Million.");
            }
        }
    }

    private double calculateMansionTaxAmount(double amount) {
        return amount * 0.01;
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    String roundOffTo2DecPlaces(double val) {
        return String.format("%.2f", val);
    }
}
