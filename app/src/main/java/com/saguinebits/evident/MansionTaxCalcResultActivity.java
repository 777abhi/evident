package com.saguinebits.evident;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Prince on 23-03-2016.
 */
public class MansionTaxCalcResultActivity extends AppCompatActivity {

    public static String EXTRA_SALE_AMOUNT = "extra_sale_amount";
    public static String EXTRA_MANSION_TAX_AMOUNT = "extra_mansion_tax_amount";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvRight)
    TextView tvRight;
    @BindView(R.id.tvMansionTaxAmountResult)
    TextView tvMansionTaxAmountResult;

    String sale_amount = "", mansion_tax_amount = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_result_mansion_tax_calc);
        ButterKnife.bind(this);

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.mansion_tax_calculator));

        Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_back);
        upArrow.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sale_amount = getIntent().getStringExtra(EXTRA_SALE_AMOUNT);
        mansion_tax_amount = getIntent().getStringExtra(EXTRA_MANSION_TAX_AMOUNT);

        tvRight.setText("$" + sale_amount);
        tvMansionTaxAmountResult.setText("$" + mansion_tax_amount);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.tvEmailEstimates)
    void click() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.email_subject_mansion_tax_calc_result));
        emailIntent.putExtra(Intent.EXTRA_BCC,new String[] { "info@evidenttitle.com","quote@evidenttitle.com"});
        emailIntent.putExtra(Intent.EXTRA_TEXT,
                getResources().getString(R.string.email_result_mansion_tax) +
                        "\n\n" +
                        "Sale Amount: $" + sale_amount +
                        "\n\n" +
                        "Mansion Tax Amount: $" + mansion_tax_amount +
                        "\n\n" +
                        getResources().getString(R.string.disclaimer_mortgage_calc));
        startActivity(Intent.createChooser(emailIntent, "Send email..."));

    }
}
