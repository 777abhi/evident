package com.saguinebits.evident;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Prince on 19-03-2016.
 */
public class MortgageCalcActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.etLoanAmount)
    EditText etLoanAmount;
    @BindView(R.id.etInterestRate)
    EditText etInterestRate;
    @BindView(R.id.tvTermYears)
    TextView tvTermYears;
    @BindView(R.id.etYearlyTaxes)
    EditText etYearlyTaxes;
    @BindView(R.id.etYearlyInsurance)
    EditText etYearlyInsurance;

    private String[] years = {"30", "20", "15", "10"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_mortgage_calculator);
        ButterKnife.bind(this);

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.mortgage_calculator));

        Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_back);
        upArrow.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick({R.id.tvTermYears, R.id.tvCalculate})
    void click(View view) {
        switch (view.getId()) {
            case R.id.tvTermYears:
                openListPopupWindow();
                break;
            case R.id.tvCalculate:

                if (etLoanAmount.getText().toString().equals("")) {
                    showToast("Please enter Loan Amount.");
                } else if (etInterestRate.getText().toString().equals("")) {
                    showToast("Please enter Interest Rate.");
                } else if (tvTermYears.getText().toString().equals("")) {
                    showToast("Please select Term Years.");
                } else {
                    String yearly_taxes = etYearlyTaxes.getText().toString();
                    String yearly_insurance = etYearlyInsurance.getText().toString();
                    if (yearly_taxes.equals("")) {
                        yearly_taxes = "0.00";
                    }
                    if (yearly_insurance.equals("")) {
                        yearly_insurance = "0.00";
                    }
                    double amount = Double.parseDouble(etLoanAmount.getText().toString());
                    double rate = Double.parseDouble(etInterestRate.getText().toString());
                    double years = Integer.parseInt(tvTermYears.getText().toString());
                    double monthly_payment = calculateMonthlyPayment(amount, rate, years);
                    double total_monthly_payment = monthly_payment + Double.parseDouble(yearly_taxes) / 12 + Double.parseDouble(yearly_insurance) / 12;
                    startActivity(new Intent(this, MortgageCalcResultActivity.class)
                            .putExtra(MortgageCalcResultActivity.EXTRA_LOAN_AMOUNT, roundOffTo2DecPlaces(amount))
                            .putExtra(MortgageCalcResultActivity.EXTRA_INTEREST_RATE, roundOffTo2DecPlaces(rate))
                            .putExtra(MortgageCalcResultActivity.EXTRA_TERM_YEARS, tvTermYears.getText().toString())
                            .putExtra(MortgageCalcResultActivity.EXTRA_YEARLY_TAXES, roundOffTo2DecPlaces(Double.parseDouble(yearly_taxes) / 12))
                            .putExtra(MortgageCalcResultActivity.EXTRA_YEARLY_INSURANCE, roundOffTo2DecPlaces(Double.parseDouble(yearly_insurance) / 12))
                            .putExtra(MortgageCalcResultActivity.EXTRA_MONTHLY_PAYMENT, roundOffTo2DecPlaces(monthly_payment))
                            .putExtra(MortgageCalcResultActivity.EXTRA_TOTAL_MONTHLY_PAYMENT, roundOffTo2DecPlaces(total_monthly_payment)));
                }
                break;
            default:
                break;
        }
    }

    private double calculateMonthlyPayment(double amount, double rate, double years) {

        return ((amount * (rate / 1200)) / (1 - (1 / Math.pow((1 + (rate / 1200)), (years * 12)))));
    }

    private void openListPopupWindow() {
        final ListPopupWindow listPopupWindow = new ListPopupWindow(this);
        listPopupWindow.setAdapter(new ArrayAdapter(this, R.layout.row_popuplist, years));
        listPopupWindow.setAnchorView(tvTermYears);
        listPopupWindow.setWidth(tvTermYears.getWidth());
        listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);
        listPopupWindow.setModal(true);
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                tvTermYears.setText(years[i]);
                listPopupWindow.dismiss();
            }
        });
        listPopupWindow.show();
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    String roundOffTo2DecPlaces(double val) {
        return String.format("%.2f", val);
    }
}
