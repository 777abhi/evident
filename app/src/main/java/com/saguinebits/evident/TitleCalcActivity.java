package com.saguinebits.evident;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Prince on 18-03-2016.
 */
public class TitleCalcActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvState)
    TextView tvState;
    @BindView(R.id.tvType)
    TextView tvType;
    @BindView(R.id.etLoan)
    EditText etLoan;
    @BindView(R.id.etAmount)
    EditText etAmount;

//    private String[] types = {"Purchase", "Refinance"};
private String[] types = {"Purchase with loan","Purchase with cash","Refinance"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_title_calculator);
        ButterKnife.bind(this);

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.title_calculator));

        Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_back);
        upArrow.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick({R.id.tvType, R.id.tvCalculate})
    void click(View view) {
        switch (view.getId()) {
            case R.id.tvType:
                openListPopupWindow();
                break;
            case R.id.tvCalculate:

                if (tvState.getText().toString().equals("")) {
                    showToast("Please enter State.");
                } else if (tvType.getText().toString().equals("")) {
                    showToast("Please select Type.");
                } else if (etAmount.getText().toString().equals("")) {
                    showToast("Please enter Amount.");
                }
               else {
                    double amount = Double.parseDouble(etAmount.getText().toString());
                    double insurance_amount = calculateInsuranceAmount(amount);

                  if(tvType.getText().toString().equalsIgnoreCase("Purchase with loan"))
                  {
                       if (etLoan.getText().toString().equals("")) {
                      showToast("Please enter Loan Amount.");
                  }
                  else{
                           double loan = Double.parseDouble(etLoan.getText().toString());
                      startActivity(new Intent(this, TitleCalcResultActivity.class)
                              .putExtra(TitleCalcResultActivity.EXTRA_STATE, tvState.getText().toString())
                              .putExtra(TitleCalcResultActivity.EXTRA_TYPE, tvType.getText().toString())
                              .putExtra(TitleCalcResultActivity.EXTRA_AMOUNT, roundOffTo2DecPlaces(amount))
                              .putExtra(TitleCalcResultActivity.EXTRA_LOAN, roundOffTo2DecPlaces(loan))
                              .putExtra(TitleCalcResultActivity.EXTRA_INSURANCE_AMOUNT, insurance_amount));

                  }}
                  else
                  {
                    startActivity(new Intent(this, TitleCalcResultActivity.class)
                            .putExtra(TitleCalcResultActivity.EXTRA_STATE, tvState.getText().toString())
                            .putExtra(TitleCalcResultActivity.EXTRA_TYPE, tvType.getText().toString())
                            .putExtra(TitleCalcResultActivity.EXTRA_AMOUNT, roundOffTo2DecPlaces(amount))
                            .putExtra(TitleCalcResultActivity.EXTRA_INSURANCE_AMOUNT, insurance_amount));
                }}
                break;
            default:
                break;
        }
    }

    private double calculateInsuranceAmount(double amount) {

        if (tvType.getText().toString().equalsIgnoreCase("Purchase with loan")) {
            if (amount <= 100000) {
                if (amount * 5.25 / 1000 > 200)
                    return amount * 5.25 / 1000;
                else
                    return 200;
            } else if (amount > 100000 && amount <= 500000) {
                return 525 + (amount - 100000) * 4.25 / 1000;
            } else if (amount > 500000 && amount <= 2000000) {
                return 2225 + (amount - 500000) * 2.75 / 1000;
            } else {
                return 6150 + (amount - 2000000) * 2.00 / 1000;
            }
        }
        else if (tvType.getText().toString().equalsIgnoreCase("Purchase with cash"))
        {   if (amount <= 100000) {
            if (amount * 5.25 / 1000 > 200)
                return amount * 5.25 / 1000;
            else
                return 200;
        } else if (amount > 100000 && amount <= 500000) {
            return 525 + (amount - 100000) * 4.25 / 1000;
        } else if (amount > 500000 && amount <= 2000000) {
            return 2225 + (amount - 500000) * 2.75 / 1000;
        } else {
            return 6150 + (amount - 2000000) * 2.00 / 1000;
        }}

        else {
            if (amount <= 100000) {
                if (amount * 2.75 / 1000 > 200)
                    return amount * 2.75 / 1000;
                else
                    return 200;
            } else if (amount > 100000 && amount <= 500000) {
                return 275 + (amount - 100000) * 2.50 / 1000;
            } else if (amount > 500000 && amount <= 2000000) {
                return 1275 + (amount - 500000) * 2.25 / 1000;
            } else {
                return 4650 + (amount - 2000000) * 1.75 / 1000;
            }
        }
    }

    private void openListPopupWindow() {

        final ListPopupWindow listPopupWindow = new ListPopupWindow(this);
        listPopupWindow.setAdapter(new ArrayAdapter(this, R.layout.row_popuplist, types));
        listPopupWindow.setAnchorView(tvType);
        listPopupWindow.setWidth(tvType.getWidth());
        listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);
        listPopupWindow.setModal(true);
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                tvType.setText(types[i]);
                if(types[i].equalsIgnoreCase("Purchase with loan"))
                {
                    etLoan.setVisibility(View.VISIBLE);
                    etAmount.setHint(R.string.purchase_amount);
                }
                else
                {
                    etLoan.setVisibility(View.GONE);
                    etAmount.setHint(R.string.amount);
                }
                listPopupWindow.dismiss();

            }
        });
        listPopupWindow.show();

    }

    String roundOffTo2DecPlaces(double val) {
        return String.format("%.2f", val);
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
