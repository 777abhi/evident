package com.saguinebits.evident;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Prince on 18-03-2016.
 */
public class TitleCalcResultActivity extends AppCompatActivity {

    public static String EXTRA_STATE = "extra_state";
    public static String EXTRA_TYPE = "extra_type";
    public static String EXTRA_AMOUNT = "extra_amount";
    public static String EXTRA_INSURANCE_AMOUNT = "extra_insurance_amount";
    public static String EXTRA_LOAN = "extra_loan";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvState)
    TextView tvState;
    @BindView(R.id.tvType)
    TextView tvType;
    @BindView(R.id.tvAmount)
    TextView tvAmount;
    @BindView(R.id.tvRight)
    TextView tvRight;
    @BindView(R.id.tvEstimatedTotalResult)
    TextView tvEstimatedTotalResult;
    @BindView(R.id.tvDisclaimer)
    TextView tvDisclaimer;

    @BindView(R.id.tvState2)
    TextView tvState2;
    @BindView(R.id.tvType2)
    TextView tvType2;
    @BindView(R.id.tvLoan2)
    TextView tvLoanAmount;

    private double estimated_total = 0;
    private String email_body = "";
    Spannable type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_result_title_calc);
        ButterKnife.bind(this);

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.title_calculator));

        Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_back);
        upArrow.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String sachy=getIntent().getStringExtra(EXTRA_TYPE);
//        switch(getIntent().getStringExtra(EXTRA_TYPE))
//        {
//            case "Purchase with loan"  :
//                tvState2.setVisibility(View.VISIBLE);
//                tvState2.setTextSize(1.1f);
//                tvType2.setVisibility(View.VISIBLE);
//                tvType2.setTextSize(1.1f);
//                tvLoanAmount.setVisibility(View.VISIBLE);
//                tvLoanAmount.setText("$ "+ getIntent().getStringExtra(EXTRA_LOAN));
//                tvLoanAmount.setTextSize(1.1f);
//                type = new SpannableString("Type" + "\n" +
//                       //getIntent().getStringExtra(EXTRA_TYPE)    );
//              R.string.purchase    );
//                type.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorPrimaryDark)), 5, 5 + getIntent().getStringExtra(EXTRA_TYPE).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                type.setSpan(new RelativeSizeSpan(1.1f), 5, 5 + getIntent().getStringExtra(EXTRA_TYPE).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                tvType.setText(type);
//                break;
//            case "Purchase with cash"  :
//              type = new SpannableString("Type" + "\n" +
//                      //getIntent().getStringExtra(EXTRA_TYPE));
//                      R.string.purchase  );
//                type.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorPrimaryDark)), 5, 5 + getIntent().getStringExtra(EXTRA_TYPE).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                type.setSpan(new RelativeSizeSpan(1.1f), 5, 5 + getIntent().getStringExtra(EXTRA_TYPE).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                tvType.setText(type);
//                break;
//            case "Refinance"  :
//                type = new SpannableString("Type" + "\n" + getIntent().getStringExtra(EXTRA_TYPE));
//                type.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorPrimaryDark)), 5, 5 + getIntent().getStringExtra(EXTRA_TYPE).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                type.setSpan(new RelativeSizeSpan(1.1f), 5, 5 + getIntent().getStringExtra(EXTRA_TYPE).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                tvType.setText(type);
//                break;
//
//        }
        if(getIntent().getStringExtra(EXTRA_TYPE).equalsIgnoreCase("Purchase with loan"))
        {
            tvState2.setVisibility(View.VISIBLE);

            tvType2.setVisibility(View.VISIBLE);

            tvLoanAmount.setVisibility(View.VISIBLE);

            tvLoanAmount.setText("$ "+ getIntent().getStringExtra(EXTRA_LOAN));
//            Spannable state = new SpannableString("State" + "\n" +
//                    //getIntent().getStringExtra(EXTRA_STATE)
//                    R.string.purchase);
//            state.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorPrimaryDark)), 6, 6 +
//                            getIntent().getStringExtra(EXTRA_STATE).length()
//                    , Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//         //   state.setSpan(new RelativeSizeSpan(1.1f), 6, 6 + getIntent().getStringExtra(EXTRA_STATE).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//            tvType.setText(state);
        }


        if(getIntent().getStringExtra(EXTRA_TYPE).equalsIgnoreCase("Refinance"))
        {
                        Spannable type = new SpannableString("Type" + "\n" + getIntent().getStringExtra(EXTRA_TYPE));
            type.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorPrimaryDark)), 5, 5 + getIntent().getStringExtra(EXTRA_TYPE).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
          //  type.setSpan(new RelativeSizeSpan(1.1f), 5, 5 + getIntent().getStringExtra(EXTRA_TYPE).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            tvType.setText(type);
        }
        else
        {
            Spannable type = new SpannableString("Type" + "\n" + getResources().getString(R.string.purchase));
            type.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorPrimaryDark)), 5, 13, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            //  type.setSpan(new RelativeSizeSpan(1.1f), 5, 5 + getIntent().getStringExtra(EXTRA_TYPE).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            tvType.setText(type);
        }
            Spannable state = new SpannableString("State" + "\n" + getIntent().getStringExtra(EXTRA_STATE));
            state.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorPrimaryDark)), 6, 6 +
                    getIntent().getStringExtra(EXTRA_STATE).length()
                    , Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
          //  state.setSpan(new RelativeSizeSpan(1.1f), 6, 6 + getIntent().getStringExtra(EXTRA_STATE).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            tvState.setText(state);

//            Spannable type = new SpannableString("Type" + "\n" + getIntent().getStringExtra(EXTRA_TYPE));
//            type.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorPrimaryDark)), 5, 5 + getIntent().getStringExtra(EXTRA_TYPE).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//          //  type.setSpan(new RelativeSizeSpan(1.1f), 5, 5 + getIntent().getStringExtra(EXTRA_TYPE).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//            tvType.setText(type);

            Spannable amount = new SpannableString("Amount" + "\n$ " + getIntent().getStringExtra(EXTRA_AMOUNT));
            amount.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorPrimaryDark)), 7, 9 + getIntent().getStringExtra(EXTRA_AMOUNT).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
         //   amount.setSpan(new RelativeSizeSpan(1.1f), 7, 9 + getIntent().getStringExtra(EXTRA_AMOUNT).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            tvAmount.setText(amount);

        switch (getIntent().getStringExtra(EXTRA_TYPE))
        {
            case "Purchase with loan":
                tvRight.setText("$ " + roundOffTo2DecPlaces(getIntent().getDoubleExtra(EXTRA_INSURANCE_AMOUNT, 0)) +
                        "\n" + getString(R.string.endorsement_cost) + "\n$545.00\n" +
                        getString(R.string.abstract_search_cost) + "\n" + getString(R.string.expense_value) + "\n" +
                        getString(R.string.courier_overnight_value));
                estimated_total = getIntent().getDoubleExtra(EXTRA_INSURANCE_AMOUNT, 0) + 1528.00f;
                tvDisclaimer.setText(getResources().getString(R.string.disclaimer_title_calc_purchase));



                email_body = getResources().getString(R.string.email_result_title) +
                        "\n\n" +
                        "State: New Jersey" +
                        "\n" +
                        "Type: " + getIntent().getStringExtra(EXTRA_TYPE) +
                        "\n" +
                        "Purchase Amount: $" + getIntent().getStringExtra(EXTRA_AMOUNT) +
                        "\n" +
                        "Loan Amount : $ " +getIntent().getStringExtra(EXTRA_LOAN) +
                        "\n" +
                        getString(R.string.insurance) + ": $" + roundOffTo2DecPlaces(getIntent().getDoubleExtra(EXTRA_INSURANCE_AMOUNT, 0)) +
                        "\n" +

                        "Endorsements: $300.00" +
                        "\n" +
                        "Settlement: $545.00" +
                        "\n" +
                        "Abstract/Search Costs: $423.00" +
                        "\n" +
                        "Expenses: " + getString(R.string.expense_value) +
                        "\n" +
                        "Courier/Overnight: " + getString(R.string.courier_overnight_value) +
                        "\n\n" +
                        "Estimated Total: $" + roundOffTo2DecPlaces(estimated_total) +
                        "\n\n" +
                        getResources().getString(R.string.disclaimer_title_calc_purchase);
                break;
            case "Purchase with cash":
                tvRight.setText("$ " + roundOffTo2DecPlaces(getIntent().getDoubleExtra(EXTRA_INSURANCE_AMOUNT, 0)) +
                        "\n" + getString(R.string.endorsement_cost_cash) + "\n$545.00\n" +
                        getString(R.string.abstract_search_cost) + "\n" + getString(R.string.expense_value) + "\n" +
                        getString(R.string.courier_overnight_value));
                estimated_total = getIntent().getDoubleExtra(EXTRA_INSURANCE_AMOUNT, 0) + 1278.00f;
                tvDisclaimer.setText(getResources().getString(R.string.disclaimer_title_calc_purchase));



                email_body = getResources().getString(R.string.email_result_title) +
                        "\n\n" +
                        "State: New Jersey" +
                        "\n" +
                        "Type: " + getIntent().getStringExtra(EXTRA_TYPE) +
                        "\n" +
                        "Loan Amount: $" + getIntent().getStringExtra(EXTRA_AMOUNT) +
                        "\n" +
                        getString(R.string.insurance) + ": $" + roundOffTo2DecPlaces(getIntent().getDoubleExtra(EXTRA_INSURANCE_AMOUNT, 0)) +
                        "\n" +
                        "Endorsements: $50.00" +
                        "\n" +
                        "Settlement: $545.00" +
                        "\n" +
                        "Abstract/Search Costs: $423.00" +
                        "\n" +
                        "Expenses: " + getString(R.string.expense_value) +
                        "\n" +
                        "Courier/Overnight: " + getString(R.string.courier_overnight_value) +
                        "\n\n" +
                        "Estimated Total: $" + roundOffTo2DecPlaces(estimated_total) +
                        "\n\n" +
                        getResources().getString(R.string.disclaimer_title_calc_purchase);

                break;

            case "Refinance":
                tvRight.setText("$ " + roundOffTo2DecPlaces(getIntent().getDoubleExtra(EXTRA_INSURANCE_AMOUNT, 0)) +
                        "\n" + getString(R.string.endorsement_cost_refiance) +
                        "\n$545.00\n$413.00\n" + getString(R.string.expense_value_refiance) + "\n" +
                        getString(R.string.courier_overnight_value));
                estimated_total = getIntent().getDoubleExtra(EXTRA_INSURANCE_AMOUNT, 0) + 1438.00f;
                tvDisclaimer.setText(getResources().getString(R.string.disclaimer_title_calc_refinance));
                email_body = getResources().getString(R.string.email_result_title) +
                        "\n\n" +
                        "State: New Jersey" +
                        "\n" +
                        "Type: " + getIntent().getStringExtra(EXTRA_TYPE) +
                        "\n" +
                        "Loan Amount: $" + getIntent().getStringExtra(EXTRA_AMOUNT) +
                        "\n" +
                        "Insurance Amount: $" + roundOffTo2DecPlaces(getIntent().getDoubleExtra(EXTRA_INSURANCE_AMOUNT, 0)) +
                        "\n" +
                        "Endorsements: $300.00" +
                        "\n" +
                        "Settlement: $545.00" +
                        "\n" +
                        "Abstract/Search Costs: $413.00" +
                        "\n" +
                        "Expenses: " + getString(R.string.expense_value_refiance) +
                        "\n" +
                        "Courier/Overnight: " + getString(R.string.courier_overnight_value) +
                        "\n\n" +
                        "Estimated Total: $" + roundOffTo2DecPlaces(estimated_total) +
                        "\n\n" +
                        getResources().getString(R.string.disclaimer_title_calc_refinance);
            break;
        }

//        if (getIntent().getStringExtra(EXTRA_TYPE).equalsIgnoreCase("Purchase with cash")) {
//            // tvRight.setText("$ " + roundOffTo2DecPlaces(getIntent().getDoubleExtra(EXTRA_INSURANCE_AMOUNT, 0)) + "\n$200.00\n$425.00\n$672.00\n$145.00\n$79.00");
//            tvRight.setText("$ " + roundOffTo2DecPlaces(getIntent().getDoubleExtra(EXTRA_INSURANCE_AMOUNT, 0)) +
//                    "\n" + getString(R.string.endorsement_cost) + "\n$545.00\n" +
//                    getString(R.string.abstract_search_cost) + "\n" + getString(R.string.expense_value) + "\n" +
//                    getString(R.string.courier_overnight_value));
//            estimated_total = getIntent().getDoubleExtra(EXTRA_INSURANCE_AMOUNT, 0) + 1528.00f;
//            tvDisclaimer.setText(getResources().getString(R.string.disclaimer_title_calc_purchase));
//
//
//            email_body = getResources().getString(R.string.email_result_title) +
//                    "\n\n" +
//                    "State: New Jersey" +
//                    "\n" +
//                    "Type: " + getIntent().getStringExtra(EXTRA_TYPE) +
//                    "\n" +
//                    "Loan Amount: $" + getIntent().getStringExtra(EXTRA_AMOUNT) +
//                    "\n" +
//                    getString(R.string.insurance) + ": $" + roundOffTo2DecPlaces(getIntent().getDoubleExtra(EXTRA_INSURANCE_AMOUNT, 0)) +
//                    "\n" +
//                    "Endorsements: $300.00" +
//                    "\n" +
//                    "Settlement: $545.00" +
//                    "\n" +
//                    "Abstract/Search Costs: $423.00" +
//                    "\n" +
//                    "Expenses: " + getString(R.string.expense_value) +
//                    "\n" +
//                    "Courier/Overnight: " + getString(R.string.courier_overnight_value) +
//                    "\n\n" +
//                    "Estimated Total: $" + roundOffTo2DecPlaces(estimated_total) +
//                    "\n\n" +
//                    getResources().getString(R.string.disclaimer_title_calc_purchase);
//        } else {
//            tvRight.setText("$ " + roundOffTo2DecPlaces(getIntent().getDoubleExtra(EXTRA_INSURANCE_AMOUNT, 0)) +
//                    "\n" + getString(R.string.endorsement_cost_refiance) +
//                    "\n$545.00\n$413.00\n" + getString(R.string.expense_value_refiance) + "\n" +
//                    getString(R.string.courier_overnight_value));
//            estimated_total = getIntent().getDoubleExtra(EXTRA_INSURANCE_AMOUNT, 0) + 1438.00f;
//            tvDisclaimer.setText(getResources().getString(R.string.disclaimer_title_calc_refinance));
//            email_body = getResources().getString(R.string.email_result_title) +
//                    "\n\n" +
//                    "State: New Jersey" +
//                    "\n" +
//                    "Type: " + getIntent().getStringExtra(EXTRA_TYPE) +
//                    "\n" +
//                    "Loan Amount: $" + getIntent().getStringExtra(EXTRA_AMOUNT) +
//                    "\n" +
//                    "Insurance Amount: $" + roundOffTo2DecPlaces(getIntent().getDoubleExtra(EXTRA_INSURANCE_AMOUNT, 0)) +
//                    "\n" +
//                    "Endorsements: $300.00" +
//                    "\n" +
//                    "Settlement: $545.00" +
//                    "\n" +
//                    "Abstract/Search Costs: $413.00" +
//                    "\n" +
//                    "Expenses: " + getString(R.string.expense_value_refiance) +
//                    "\n" +
//                    "Courier/Overnight: " + getString(R.string.courier_overnight_value) +
//                    "\n\n" +
//                    "Estimated Total: $" + roundOffTo2DecPlaces(estimated_total) +
//                    "\n\n" +
//                    getResources().getString(R.string.disclaimer_title_calc_refinance);
//        }

        tvEstimatedTotalResult.setText("$ " + roundOffTo2DecPlaces(estimated_total));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.tvEmailResults)
    void click() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "", null));
        emailIntent.putExtra(Intent.EXTRA_BCC,new String[] { "info@evidenttitle.com","quote@evidenttitle.com"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.email_subject_title_calc_result));
        emailIntent.putExtra(Intent.EXTRA_TEXT, email_body);
        startActivity(Intent.createChooser(emailIntent, "Send email..."));

    }

    String roundOffTo2DecPlaces(double val) {
        return String.format("%.2f", val);
    }
}
