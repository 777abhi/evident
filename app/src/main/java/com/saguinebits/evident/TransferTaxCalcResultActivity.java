package com.saguinebits.evident;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Prince on 19-03-2016.
 */
public class TransferTaxCalcResultActivity extends AppCompatActivity {

    public static String EXTRA_SALE_AMOUNT = "extra_sale_amount";
    public static String EXTRA_EXEMPTION = "extra_exemption";
    public static String EXTRA_PROPERTY_TYPE = "extra_property_type";
    public static String EXTRA_TRANSFER_TAX_AMOUNT = "extra_transfere_tax_amount";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvRight)
    TextView tvRight;
    @BindView(R.id.tvTransferTaxAmountResult)
    TextView tvTransferTaxAmountResult;

    String sale_amount = "", exemption = "", property_type = "", transfer_tax_amount = "";
    private Html.ImageGetter imgGetter = new Html.ImageGetter() {

        public Drawable getDrawable(String source) {
            Drawable drawable = ContextCompat.getDrawable(TransferTaxCalcResultActivity.this, R.drawable.ic_app_icon);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable
                    .getIntrinsicHeight());

            return drawable;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_result_transfer_tax_calc);
        ButterKnife.bind(this);

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.transfer_tax_calculator));

        Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_back);
        upArrow.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sale_amount = getIntent().getStringExtra(EXTRA_SALE_AMOUNT);
        exemption = getIntent().getStringExtra(EXTRA_EXEMPTION);
        property_type = getIntent().getStringExtra(EXTRA_PROPERTY_TYPE);
        transfer_tax_amount = getIntent().getStringExtra(EXTRA_TRANSFER_TAX_AMOUNT);

        tvRight.setText("$ " + sale_amount + "\n" + exemption + "\n" + property_type);
        tvTransferTaxAmountResult.setText("$ " + transfer_tax_amount);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.tvEmailEstimates)
    void click() {
        String body = getResources().getString(R.string.email_result_transfer_tax) +
                "\n\n" +
                "Sale Amount: $" + sale_amount +
                "\n" +
                "Exemption: " + exemption +
                "\n" +
                "Property Type: " + property_type +
                "\n\n" +
                "Transfer Tax Amount: $" + transfer_tax_amount +
                "\n\n" +
                getResources().getString(R.string.disclaimer_mortgage_calc);

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "", null));
        emailIntent.putExtra(Intent.EXTRA_BCC,new String[] { "info@evidenttitle.com","quote@evidenttitle.com"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.email_subject_transfer_tax_calc_result));
        emailIntent.putExtra(Intent.EXTRA_TEXT, body);
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

}
