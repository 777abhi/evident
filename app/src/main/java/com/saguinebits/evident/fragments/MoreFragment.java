package com.saguinebits.evident.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.saguinebits.evident.R;
import com.saguinebits.evident.WebviewActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MoreFragment extends Fragment {
//@BindView(R.id.toolbar)
//    Toolbar toolbar;
//    @BindView(R.id.tvBottomText)
//    TextView tvMido;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more, container, false);
        ButterKnife.bind(this, view);
//        toolbar.setTitleTextColor(Color.WHITE);
//
//        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.more));
        Spannable text = new SpannableString(getResources().getString(R.string.more_mido_text));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), WebviewActivity.class)
                        .putExtra(WebviewActivity.EXTRA_TITLE, getResources().getString(R.string.mido_group))
                        .putExtra(WebviewActivity.EXTRA_LINK, getResources().getString(R.string.mido_website_link)));

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        text.setSpan(clickableSpan, 26, 36, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        text.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.theme_yellow)), 26, 36, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        tvMido.setText(text);
//        tvMido.setMovementMethod(LinkMovementMethod.getInstance());

        return view;
    }

    @OnClick({R.id.tvVisitWebsite, R.id.tvCallUs, R.id.tvEmailUs})
    void click(View view) {
        switch (view.getId()) {
            case R.id.tvVisitWebsite:
                startActivity(new Intent(getActivity(), WebviewActivity.class)
                        .putExtra(WebviewActivity.EXTRA_LINK, getResources().getString(R.string.website_link))
                        .putExtra(WebviewActivity.EXTRA_TITLE, getResources().getString(R.string.visit_our_website)));
                break;
            case R.id.tvCallUs:

                if (((TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE)).getPhoneType()
                        == TelephonyManager.PHONE_TYPE_NONE)
                {
                    Toast.makeText(getActivity(),"No Calling App Found",Toast.LENGTH_LONG).show();
                }

                else
                {
                    String phone = getActivity().getResources().getString(R.string.phone_no);
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" +phone));
                    startActivity(intent);
                }
                break;
            case R.id.tvEmailUs:
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto"," ", null));
                emailIntent.putExtra(Intent.EXTRA_BCC,new String[] { "info@evidenttitle.com","quote@evidenttitle.com"});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
                break;
//            case R.id.tvOurLocations:
//                startActivity(new Intent(getActivity(), WebviewActivity.class)
//                        .putExtra(WebviewActivity.EXTRA_LINK, getResources().getString(R.string.our_locations_link))
//                        .putExtra(WebviewActivity.EXTRA_TITLE, getResources().getString(R.string.our_locations)));
//                break;
            default:
                break;
        }
    }
}
