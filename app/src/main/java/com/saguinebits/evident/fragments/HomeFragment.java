package com.saguinebits.evident.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.saguinebits.evident.R;
import com.saguinebits.evident.WebviewActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.content.Context.TELEPHONY_SERVICE;

public class HomeFragment extends Fragment {

    private Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);

 //       TextView tvCall = (TextView) view.findViewById(R.id.tvCall);
//        tvCall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String phone = getActivity().getResources().getString(R.string.phone_no);
//                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
//                startActivity(intent);
//            }
//        });
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
    private boolean isTelephonyEnabled(){
        TelephonyManager tm = (TelephonyManager)getActivity().getSystemService(TELEPHONY_SERVICE);
        return tm != null && tm.getSimState()==TelephonyManager.SIM_STATE_READY;
    }
    @OnClick({R.id.tvCall, R.id.tvEmail, R.id.tvWeb})
    void click(View view) {
        switch (view.getId()) {
            case R.id.tvCall:

                if (((TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE)).getPhoneType()
                        == TelephonyManager.PHONE_TYPE_NONE)
                {
                    Toast.makeText(getActivity(),"No Calling App Found",Toast.LENGTH_LONG).show();
                }
             else
                {
                    String phone = getActivity().getResources().getString(R.string.phone_no);
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" +phone));
                    startActivity(intent);
                }

                break;
            case R.id.tvEmail:
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto"," ", null));
                emailIntent.putExtra(Intent.EXTRA_BCC,new String[] { "info@evidenttitle.com","quote@evidenttitle.com"});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
                break;
            case R.id.tvWeb:
                startActivity(new Intent(getActivity(), WebviewActivity.class)
                        .putExtra(WebviewActivity.EXTRA_TITLE, getResources().getString(R.string.our_website))
                        .putExtra(WebviewActivity.EXTRA_LINK, getResources().getString(R.string.website_link)));
                break;
            default:
                break;
        }
    }
}
