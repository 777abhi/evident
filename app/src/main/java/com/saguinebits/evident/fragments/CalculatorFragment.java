package com.saguinebits.evident.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.saguinebits.evident.MansionTaxCalcActivity;
import com.saguinebits.evident.MortgageCalcActivity;
import com.saguinebits.evident.R;
import com.saguinebits.evident.TitleCalcActivity;
import com.saguinebits.evident.TransferTaxCalcActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CalculatorFragment extends Fragment {

//    @BindView(R.id.toolbar)
//    Toolbar toolbar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calculator, container, false);
        ButterKnife.bind(this, view);

//        toolbar.setTitleTextColor(Color.WHITE);
//        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.calculators));
        return view;
    }

    @OnClick({R.id.tvTitleCalc, R.id.tvMortgageCalc, R.id.tvTransferTaxCalc, R.id.tvMansionTaxCalc})
    void click(View view) {
        switch (view.getId()) {
            case R.id.tvTitleCalc:
                startActivity(new Intent(getActivity(), TitleCalcActivity.class));
                break;
            case R.id.tvMortgageCalc:
                startActivity(new Intent(getActivity(), MortgageCalcActivity.class));
                break;
            case R.id.tvTransferTaxCalc:
                startActivity(new Intent(getActivity(), TransferTaxCalcActivity.class));
                break;
            case R.id.tvMansionTaxCalc:
                startActivity(new Intent(getActivity(), MansionTaxCalcActivity.class));
                break;
            default:
                break;
        }
    }

}
